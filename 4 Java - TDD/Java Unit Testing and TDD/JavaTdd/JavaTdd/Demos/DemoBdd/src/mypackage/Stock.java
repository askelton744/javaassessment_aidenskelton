package mypackage;

public class Stock {

	private String symbol;
	private double threshold;
	private double tradedAt = -1;
	
	public Stock(String symbol, double threshold) {
		this.symbol = symbol;
		this.threshold = threshold;
	}
	
	public void tradeAt(double price) {
		this.tradedAt = price;
	}
	
	public String getStatus() {
		if (tradedAt == -1) 
			return "NOT_TRADED_YET";
		else if (tradedAt <= threshold)
			return "OFF";
		else 
			return "ON"; 
	}
}
