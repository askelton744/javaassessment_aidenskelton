package stubsandmocks.version3; 

import java.util.HashSet;
import java.util.Set;

public class Lottery {

	private static final int LOTTERY_SIZE = 6;
	private static final int HIGHEST_NUMBER = 49; 

	// Constructor injection
	private NumberGenerator generator; 

	public Lottery(NumberGenerator generator) {
		this.generator = generator; 
	}

	Set<Integer> generateRandomSet() {
		Set<Integer> numbers = new HashSet<Integer>(LOTTERY_SIZE);
		while (numbers.size() < LOTTERY_SIZE) {
			numbers.add(generator.generate(HIGHEST_NUMBER));
		}
		return numbers;
	}
	
	String formatNumbers(Set<Integer> numbers) {
		int count = 0; 
		StringBuilder sb = new StringBuilder();	
		for (Integer number : numbers) {
			sb.append(number);
			if (++count < numbers.size()) sb.append(" - ");	
		}
		return sb.toString();
	}
}