package stubsandmocks.version3; 

public interface NumberGenerator {
	public int generate(int limit); 
}