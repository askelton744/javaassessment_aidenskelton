package stubsandmocks.version3;

import static org.easymock.EasyMock.anyInt;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

// Illustrate contrast between stub and mock 
public class LotteryTest_MocksVsStubs {

	// As before, can assert that the random number is within a range
	public void generateGivenLimit10ReturnsNumberInRange() {
		NumberGenerator generator = new RandomNumberGenerator();
		for (int i = 0; i < 10; i++) {
			assertThat(generator.generate(10),
					allOf(greaterThan(0), lessThanOrEqualTo(10)));
		}
	}
	
	@Test 
	public void formatIsHyphenSeparatedSequenceOfNumbers() {
		Lottery lotto = new Lottery(null);
		Set<Integer> numbers = new HashSet<Integer>();
		numbers.addAll(Arrays.asList(20, 30, 40));
		assertTrue(lotto.formatNumbers(numbers).matches("^\\d+ - \\d+ - \\d+$"));
	}

	// How to do it with a mock
	@Test
	public void generateRandomSetReturnsLotteryNumbers() {
		NumberGenerator mockGenerator = createMock(NumberGenerator.class);
		// instead of a series of 6 of these: 
		// expect(mockGenerator.generate(anyInt())).andReturn(1);
		for (int i = 1; i < 7; i++) {
			expect(mockGenerator.generate(anyInt())).andReturn(i);			
		}
		replay(mockGenerator);

		Lottery lotto = new Lottery(mockGenerator);
		Set<Integer> numbers = lotto.generateRandomSet();
		assertThat(lotto.formatNumbers(numbers), is("1 - 2 - 3 - 4 - 5 - 6"));
		
		verify(mockGenerator);
	}
}