package stubsandmocks.version2;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

// How can you test when randomness is involved?
public class LotteryTest {

	private Lottery lotto;
	
	@Before
	public void setUp() {
		NumberGenerator generatorStub = new NumberGeneratorStub();
		lotto = new Lottery(generatorStub);
	}

	// Assert that numbers are formatted correctly.
	@Test 
	public void formatIsHyphenSeparatedSequenceOfNumbers() {
		Set<Integer> numbers = new HashSet<Integer>();
		numbers.addAll(Arrays.asList(20, 30, 40));
		assertTrue(lotto.formatNumbers(numbers).matches("^\\d+ - \\d+ - \\d+$"));
	}

	// Assert that the random number is within a range, using the real RandomNumberGenerator class. 
	public void generateGivenLimit10ReturnsNumberInRange() {
		NumberGenerator generator = new RandomNumberGenerator();
		for (int i = 0; i < 10; i++) {
			assertThat(generator.generate(10), allOf(greaterThan(0), lessThanOrEqualTo(10)));
		}
	}

	// More importantly, use the stub to test the generateRandomSet() method.
	@Test
	public void generateRandomSetReturnsLotteryNumbers() {
		Set<Integer> numbers = lotto.generateRandomSet();
		String expected = "0 - 1 - 2 - 3 - 4 - 5";
		assertThat(lotto.formatNumbers(numbers), is(expected));
	}
}