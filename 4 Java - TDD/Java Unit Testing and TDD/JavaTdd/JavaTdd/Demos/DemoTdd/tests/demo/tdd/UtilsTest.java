package demo.tdd;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class UtilsTest {
	
	private int input;
	private String expected;

	@Parameters
	public static Collection<?> makeData() {
		return Arrays.asList( new Object[][] {
                               { 1,  "Jan" },
                               { 2,  "Feb" }, 
                               { 3,  "Mar" }, 
                               { 4,  "Apr" }, 
                               { 5,  "May" }, 
                               { 6,  "Jun" }, 
                               { 7,  "Jul" }, 
                               { 8,  "Aug" }, 
                               { 9,  "Sep" }, 
                               { 10, "Oct" }, 
                               { 11, "Nov" },                                
                               { 12, "Dec" },
                               });
	}               // { input to function, and expected output }

	public UtilsTest(int input, String output) {
		this.input = input;
		this.expected = output;
	}

	@Test
	  public void testGetMonthString() {
	    assertEquals("Bad month string", expected, 
	                  Utils.getMonthString(input));
	  }
}
