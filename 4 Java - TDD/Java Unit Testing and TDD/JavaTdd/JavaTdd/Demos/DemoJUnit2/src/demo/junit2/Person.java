package demo.junit2;

public class Person implements Comparable<Person> {

    private String givenName;
    private String familyName;
    private int age;
    
    public Person(String givenName, String familyName, int age) {
		this.givenName = givenName;
		this.familyName = familyName;
		this.age = age;
	}

	public String getGivenName() {
		return givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public int getAge() {
		return age;
	}

	@Override
    public int compareTo(Person other) {
        int otherAge = other.age;
        return this.age - otherAge;
    } 

    @Override
    public String toString() {
        return familyName + ", " + givenName;
    }
}
