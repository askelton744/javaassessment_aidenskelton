package util.part5;

// Method is getting long; has some code duplication ...
public class SecondsToWords {

	public static String convert(int seconds) {
		String rv = "";
		int hours = seconds / 3600;
		if (hours > 0) {
			rv += hours + " hours, ";
			seconds = seconds % 3600;
		}
		int mins = seconds / 60;
		seconds = seconds % 60;
		if (mins > 0) {
			String minEnd = (mins == 1) ? "" : "s";
			rv += mins + " minute" + minEnd;
			if (seconds == 0) return rv; 
			rv += ", ";			
		}
		String secEnd = (seconds == 1) ? "" : "s";
		return rv + seconds + " second" + secEnd;
	}

}
