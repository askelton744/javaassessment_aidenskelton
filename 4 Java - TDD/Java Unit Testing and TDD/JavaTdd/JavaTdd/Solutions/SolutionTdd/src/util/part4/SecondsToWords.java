package util.part4;

public class SecondsToWords {

	public static String convert(int seconds) {
		String rv = "";
		int mins = seconds / 60;
		seconds %= 60;
		if (mins > 0) {
			rv = pluralize("minute", mins);
			if (seconds == 0) return rv; 
			rv += ", ";			
		}
		return rv + pluralize("second", seconds);
	}

	static String pluralize(String word, int number) {
		String suffix = (number == 1) ? "" : "s";
		return number + " " + word + suffix;
	}
}
