package util.part3;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static util.part3.SecondsToWords.convert;

import org.junit.Test;

// Now basic minutes
public class SecondsToWordsTest {
	
	@Test public void convertZeroSeconds() {
		assertThat(convert(0), is("0 seconds"));
	}
	
	@Test public void convertLessThan60Seconds() {
		assertThat(convert(2), is("2 seconds"));
	}

	@Test public void convertSingularSecs() {
		assertThat(convert(1), is("1 second"));
	}
	
	@Test public void convertPluralMinsPluralSecs() {
		assertThat(convert(122), is("2 minutes, 2 seconds"));
	}

	@Test public void convertSingularMinsPluralSecs() {
		assertThat(convert(62), is("1 minute, 2 seconds"));
	}

	@Test public void convertSingularMinsSingularSecs() {
		assertThat(convert(61), is("1 minute, 1 second"));
	}
	
	
	// After refactoring - remove duplication: 
	
	@Test public void pluralizeSingular() {
		assertThat(SecondsToWords.pluralize("dog", 1), is("1 dog"));
	}
	
	@Test public void pluralizePlural() {
		assertThat(SecondsToWords.pluralize("cat", 2), is("2 cats"));
	}
}
