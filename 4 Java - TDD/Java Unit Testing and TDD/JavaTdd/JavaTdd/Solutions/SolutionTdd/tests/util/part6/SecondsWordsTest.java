package util.part6;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static util.part6.SecondsToWords.convert;

import org.junit.Test;

// Other hours cases
public class SecondsWordsTest {
	
	@Test public void convertZeroSecs() {
		assertThat(convert(0), is("0 seconds"));
	}
	
	@Test public void convertLessThan60Secs() {
		assertThat(convert(2), is("2 seconds"));
	}

	@Test public void convertHandlesSingularSec() {
		assertThat(convert(1), is("1 second"));
	}
	
	@Test public void convert2MinsPlus() {
		assertThat(convert(122), is("2 minutes, 2 seconds"));
	}

	@Test public void convertHandlesSingularMinPluralSecs() {
		assertThat(convert(62), is("1 minute, 2 seconds"));
	}

	@Test public void convert1Min1Sec() {
		assertThat(convert(61), is("1 minute, 1 second"));
	}
	
	@Test public void convert1WholeMin() {
		assertThat(convert(60), is("1 minute"));
	}
	
	@Test public void convert3WholeMins() {
		assertThat(convert(180), is("3 minutes"));
	}
	
	@Test public void convert2HoursPlus() {
		assertThat(convert(11045), is("3 hours, 4 minutes, 5 seconds"));
	}

	@Test public void convert1Hour1Min1Sec() {
		assertThat(convert(3661), is("1 hour, 1 minute, 1 second"));
	}
	
	@Test public void convert5WholeHours() {
		assertThat(convert(18000), is("5 hours"));
	}
	
	@Test public void convertWholeHoursWholeMins() {
		assertThat(convert(18060), is("5 hours, 1 minute"));
	}

	@Test public void pluralisePlural() {
		assertThat(SecondsToWords.pluralise(2, "bird"), is("2 birds"));		
	}
	
	@Test public void pluraliseSingular() {
		assertThat(SecondsToWords.pluralise(1, "bird"), is("1 bird"));		
	}
}
