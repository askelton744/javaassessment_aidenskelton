package util.part2;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static util.part2.SecondsToWords.convert;

import org.junit.Test;

// Now basic minutes
public class SecondsToWordsTest {
	
	@Test public void convertZeroSeconds() {
		assertThat(convert(0), is("0 seconds"));
	}
	
	@Test public void convertLessThan1Minute() {
		assertThat(convert(2), is("2 seconds"));
	}

	@Test public void convertSingularSecs() {
		assertThat(convert(1), is("1 second"));
	}

}
