package employees.part3;

import static employees.part3.Country.US;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Test;

public class ManagerTest {
	
	// E for your first year; then F, then G, then over 3 years H 
	@Test
	public void managerHiredForMoreThan4YearsIsAtTopGrade() {
		Employee helen = createHelen();
		assertThat(helen.getGrade(), is(PayGrade.H));
	}

	@Test
	public void managerHiredForMoreThan4YearsHasTopSalary() {
		Employee mary = createHelen();
		assertThat(mary.getSalary(), is(25000));
	}

	private Employee createHelen() {
		Address main = new Address("1001 Main St", "Springville", "12345", US);
		Calendar hired = Calendar.getInstance();
		hired.add(Calendar.YEAR, -4);
		hired.add(Calendar.MONTH, -8);
		return new Manager("Helen Hoggs", hired, "XY 98 76 54 Z", main);
	}

}
