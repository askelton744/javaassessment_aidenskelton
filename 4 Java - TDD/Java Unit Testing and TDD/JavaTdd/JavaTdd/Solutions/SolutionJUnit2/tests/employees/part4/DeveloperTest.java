package employees.part4;

import static employees.part4.Country.UK;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Test;

public class DeveloperTest {
	
	@Test
	public void developerHiredForMoreThan2YearsIsAtGradeD() {
		Employee mary = createMary();
		assertThat(mary.getGrade(), is(PayGrade.D));
	}

	@Test
	public void developerHiredForMoreThan2YearsHasSalary21K() {
		Employee mary = createMary();
		assertThat(mary.getSalary(), is(21000));
	}

	@Test
	public void developerHiredForMoreThan2YearsGetsBonus300() {
		Employee mary = createMary();
		assertThat(mary.getBonus(), is(300));
	}

	private Employee createMary() {
		Address tortilla = new Address("15B Tortilla Lane", "Birmingham", "B5 9DG", UK);
		Calendar hired = Calendar.getInstance();
		hired.add(Calendar.YEAR, -2);
		hired.add(Calendar.MONTH, -10);
		return new Developer("Mary Moggs", hired, "CD 33 22 44 Y", tortilla);
	}
}
