package employees.part3;

import static employees.part3.Country.UK;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Test;

public class DeveloperTest {

	// B for your first year; C for your second year; D once you have 
	// worked more than 2 years 
	@Test
	public void developerHiredForMoreThan2YearsIsAtGradeD() {
		Employee mary = createMary();
		assertThat(mary.getGrade(), is(PayGrade.D));
	}

	@Test
	public void developerHiredForMoreThan2YearsHasSalary21K() {
		Employee mary = createMary();
		assertThat(mary.getSalary(), is(21000));
	}

	// Mary's Calendar hired is 2 years and 10 months back  
	private Employee createMary() {
		Address tortilla = new Address("15B Tortilla Lane", "Birmingham", "B5 9DG", UK);
		Calendar hired = Calendar.getInstance();
		hired.add(Calendar.YEAR, -2);
		hired.add(Calendar.MONTH, -10);
		return new Developer("Mary Moggs", hired, "CD 33 22 44 Y", tortilla);
	}
}
