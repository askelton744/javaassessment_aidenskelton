package employees.part3;

import static employees.part3.Country.UK;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Test;

// These test the concrete functionality in the abstract Employee class
// i.e. we will need an Apprentice etc. to create an instance to test
public class EmployeeTest {
	
	@Test
	public void employeeStringForm() {
		Employee jim = createJim();
		assertThat(jim.toString(), is("Jim Joggs [12 Acacia Avenue, London, E10 7RJ]"));
	}
  
	@Test
	public void getLocationReturnsAddressCountry() {
		Employee jim = createJim();
		assertThat(jim.getLocation(), is(UK));
	}
  
	@Test
	public void ssnDeterminesEmployeeIdentity() {
		Employee jim = createJim();
		Employee otherJim = createJimSameButDifferent();
		assertEquals(otherJim, jim);
	}

	@Test
	public void equalsEmployeesHaveSameHashCode() {
		Employee jim = createJim();
		Employee otherJim = createJimSameButDifferent();
		assertEquals("A prerequisite for the test", otherJim, jim);
		assertEquals(otherJim.hashCode(), jim.hashCode());
	}
  
	// By making the Calendar hired "7 months ago" - i.e. relative to now -
	// whenever we run this test the logic will be correct.  
	private Employee createJim() {
		Address acacia = new Address("12 Acacia Avenue", "London", "E10 7RJ", UK);
		Calendar hired = Calendar.getInstance();
		hired.add(Calendar.MONTH, -7);
		return new Apprentice("Jim Joggs", hired, "AB 12 34 56 P", acacia);
	}

	private Employee createJimSameButDifferent() {
		Address different = new Address("221B Baker Street", "London", "WC2 1HG", UK);
		Calendar cal = Calendar.getInstance();
		Employee otherJim = new Apprentice("Jimmy Joggs", cal, "AB 12 34 56 P", different);
		return otherJim;
	}
}
