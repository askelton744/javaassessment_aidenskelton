package employees.part3;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import employees.part3.PayGrade;
import static employees.part3.PayGrade.G;

public class PayGradeTest {

	@Test
	public void getSalaryPayGradeA() {
		assertThat(PayGrade.A.getSalary(), is(18000));
	}

	@Test
	public void getSalaryPayGradeG() {
		assertThat(G.getSalary(), is(24000));
	}
}
