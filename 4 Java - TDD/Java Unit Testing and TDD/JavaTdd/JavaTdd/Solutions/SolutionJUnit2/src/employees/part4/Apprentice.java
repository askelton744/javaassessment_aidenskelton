package employees.part4;

import java.util.Calendar;

public class Apprentice extends Employee {

	public Apprentice(String name, Calendar start, String ssn, Address home) {
		super(name, start, ssn, home);
	}

	// If they started more than a year ago, their grade is B
	@Override
	protected PayGrade calculateGrade() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -1);
		return (start.before(cal)) ? PayGrade.B : PayGrade.A;
	}

	@Override
	public int getBonus() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -6);
		return (start.before(cal)) ? 100 : 0;
	}

}
