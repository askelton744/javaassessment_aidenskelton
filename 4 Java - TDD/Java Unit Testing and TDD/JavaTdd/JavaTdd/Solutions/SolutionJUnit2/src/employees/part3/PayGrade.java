package employees.part3;

public enum PayGrade {
	
	A(18000), 
	B(19000), 
	C(20000), 
	D(21000), 
	E(22000), 
	F(23000), 
	G(24000), 
	H(25000);
	
	private int salary; 
	private PayGrade(int salary) {
		this.salary = salary;
	}
	
	public int getSalary() {
		return salary;
	}
}
