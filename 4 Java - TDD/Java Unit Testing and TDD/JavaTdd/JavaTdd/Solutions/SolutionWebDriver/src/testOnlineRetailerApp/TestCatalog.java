package testOnlineRetailerApp;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class TestCatalog {

	private WebDriver driver;
	
	@Before
	public void setup() {
		 driver = new HtmlUnitDriver();
	}
	
	@Test
	public void onlineRetailerApp_addItemToBasket_basketContainsNewItem() {

		driver.get("http://localhost:8080/OnlineRetailerApp/CatalogServlet");

		// Get the ProductID and Quantity text boxes. Also get the Add button.
		WebElement productID = driver.findElement(By.name("ProductID"));
		WebElement quantity = driver.findElement(By.name("Quantity"));
		WebElement add = driver.findElement(By.name("Add"));
		
		// Enter product ID and quantity. 
		productID.sendKeys("1");
		quantity.sendKeys("100");
		add.click();
		
		// Verify the basket contains an item now. 
		WebElement table = driver.findElement(By.xpath("//h2[text()='Current contents of basket:']/following-sibling::table"));
		List<WebElement> productRows = table.findElements(By.tagName("tr"));
		assertThat(productRows.size(), equalTo(1));

		// Verify the item product and quantity are correct. 
		List<WebElement> cols = productRows.get(0).findElements(By.tagName("td"));
		assertThat(cols.get(0).getText().replace(" ", ""), equalTo("ProductID:1"));
		assertThat(cols.get(1).getText().replace(" ", ""), equalTo("Quantity:100"));
	}
}
