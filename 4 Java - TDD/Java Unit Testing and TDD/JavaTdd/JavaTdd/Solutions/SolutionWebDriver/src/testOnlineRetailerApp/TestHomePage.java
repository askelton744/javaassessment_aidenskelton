package testOnlineRetailerApp;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class TestHomePage {

	private WebDriver driver;
	
	@Before
	public void setup() {
		 driver = new HtmlUnitDriver();
	}
	
	@Test
	public void onlineRetailerApp_goodUrl_displaysHomePage() {
		driver.get("http://localhost:8080/OnlineRetailerApp/Login.jsp");
		assertThat(driver.getTitle(), equalTo("Login Page"));
	}

	@Test
	public void onlineRetailerApp_badUrl_displaysGlassfishError() {
		driver.get("http://localhost:8080/OnlineRetailerApp/ThisIsABadUrl.jsp");
		assertThat(driver.getTitle(), 
				   allOf(startsWith("GlassFish Server"), endsWith("Error report")));
	}
	
	@Test
	public void onlineRetailerApp_homePage_displaysLoginFields() {

		driver.get("http://localhost:8080/OnlineRetailerApp/Login.jsp");

		// Check we see login form.
		List<WebElement> elts = driver.findElements(By.tagName("form"));	// We don't get exception if not found, so need to check:
		assertThat("Page should contain 1 form", elts.size(), equalTo(1));

		// Find CustomerID, Name, and Address input fields.
		//  - Don't catch exceptions, missing element exception will cause test to fail
		driver.findElement(By.name("CustomerID"));
		driver.findElement(By.name("Name"));
		driver.findElement(By.name("Address"));
	}
}
