package testOnlineRetailerApp;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class TestLogin {

	private WebDriver driver;
	
	@Before
	public void setup() {
		 driver = new HtmlUnitDriver();
	}
	
	@Test
	public void onlineRetailerApp_enterLoginInfo_showsEnrolledPage() {

		driver.get("http://localhost:8080/OnlineRetailerApp/Login.jsp");

		// Find CustomerID, Name, and Address input fields.
		//  - Don't catch exceptions, missing element exception will cause test to fail.
		WebElement form = driver.findElement(By.tagName("form"));	
		WebElement customerID = driver.findElement(By.name("CustomerID"));
		WebElement name = driver.findElement(By.name("Name"));
		WebElement address = driver.findElement(By.name("Address"));

		// Enter customer ID, name, and address. Then submit the form.
		customerID.sendKeys("1");
		name.sendKeys("Smith");
		address.sendKeys("London");
		form.submit();

		// Verify the next page is displayed, with a suitable <h1> heading. 
		WebElement h1 = driver.findElement(By.tagName("h1"));
		assertThat(h1.getText(), equalTo("Successfully enrolled!"));
	}
	
	@Test
	public void onlineRetailerApp_clickCatalogLink_showsCatalogPage() {

		driver.get("http://localhost:8080/OnlineRetailerApp/EnrolServlet?CustomerID=1&Name=Smith&Address=London");

		// Find hyperlink.
		WebElement link = driver.findElement(By.tagName("a"));	

		// Click hyperlink.
		link.click();

		// Verify the next page is displayed, with 7 products. 
		WebElement table = driver.findElement(By.xpath("//h2/following-sibling::table"));
		List<WebElement> productRows = table.findElements(By.tagName("tr"));
		assertThat(productRows.size(), equalTo(7));
	}
}
