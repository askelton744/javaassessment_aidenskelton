package menusystem.part7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

@SuppressWarnings("unused")
public class Demo {

	private PrintStream out;
	private BufferedReader reader; 
	private User user; 

	public Demo(PrintStream out, BufferedReader reader) {
		this.out = out;
		this.reader = reader; 
	}

	String mainMenu() {
		StringBuffer output = new StringBuffer();
		output.append("1: Main menu (this)\n");
		output.append("2: Registration\n");
		output.append("4: Exit\n");
		return output.toString();
	}

	public void run() throws IOException {
		out.println("Please enter a number:");
		out.println(mainMenu());
		readingInput: while (true) {
			int choice = readAnInt();
			
			switch (choice) {
				case 1:
					out.println(mainMenu());
					break;
				case 2:
					user = registerUser();
					break;
				case 4:
					break readingInput;
				default:
					out.println("Sorry, unrecognised input.  Please try again");
					break;
			}
		}
		out.println("Thank you for using the system");
	}

	int readAnInt() throws IOException {
		String line = reader.readLine(); 
		int choice = -1;
		try {
			choice = Integer.parseInt(line);
		} 
		catch (NumberFormatException e) {
		}
		return choice;
	}

	User registerUser() throws IOException {
		out.println("Please choose a user name:");
		String name = reader.readLine(); 
		out.println("Please choose a password:");
		String pwd = reader.readLine();
		return new User(name, pwd);
	}
}
