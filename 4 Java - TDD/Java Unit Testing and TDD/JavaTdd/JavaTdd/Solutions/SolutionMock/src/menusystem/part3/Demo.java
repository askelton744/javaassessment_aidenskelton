package menusystem.part3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

public class Demo {

	private PrintStream out;
	private BufferedReader reader; 

	public Demo(PrintStream out, BufferedReader reader) {
		this.out = out;
		this.reader = reader; 
	}

	String mainMenu() {
		StringBuffer output = new StringBuffer();
		output.append("1: Main menu (this)\n");
		output.append("4: Exit\n");
		return output.toString();
	}

	public void run() throws IOException {
		out.println(mainMenu());
		reader.readLine();
		out.println("Thank you for using the system");
	}
}
