package menusystem.part5;

import static org.easymock.EasyMock.endsWith;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.startsWith;
import static org.junit.Assert.assertThat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import org.easymock.EasyMockSupport;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

// Test which in effect requires while loop for reading input:
// specify scenario where user requests main menu a 2nd time
// introduction of while loop requires some tests to be re-written to 
// break out of loop, otherwise they never return
// DRY in tests: extract expectUserExits()
public class DemoTest extends EasyMockSupport {

	private Demo demo;
	private PrintStream mockOut;
	private BufferedReader mockReader; 


	@Before
	public void setUp() throws Exception {
		mockOut = createNiceMock(PrintStream.class);
		mockReader = createNiceMock(BufferedReader.class);
		demo = new Demo(mockOut, mockReader);
	}

	@Test
	public void mainMenuContent() {
		assertThat(demo.mainMenu(), Matchers.startsWith("1: Main menu (this)\n"));
		assertThat(demo.mainMenu(), Matchers.endsWith("4: Exit\n"));
	}

	// @Test run_prints_mainMenu_toConsole has become redundant
	// the necessary insertion of expectUserExits to exit the while 
	// loop makes it identical to this: 

	@Test
	public void userExitsDemoImmediately() throws IOException {
		expectInitialMenu();
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}

	@Test
	public void readAnIntReturnsValidInt() throws IOException {
		expect(mockReader.readLine()).andReturn("1");
		replayAll();
		
		int entered = demo.readAnInt();
		verifyAll();
		assertThat(entered, Matchers.is(1));
	}
	
	@Test
	public void readAnIntGivenInvalidReturnsMinus1() throws IOException {
		expect(mockReader.readLine()).andReturn("");
		replayAll();
		
		int entered = demo.readAnInt();
		verifyAll();
		assertThat(entered, Matchers.is(-1));
	}

	@Test
	public void userRequestsMainMenu() throws IOException {
		expectInitialMenu();
		expect(mockReader.readLine()).andReturn("1");
		mockOut.println(startsWith("1: Main menu (this)\n"));
		expectLastCall(); 
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}

	@Test
	public void userRequestsMainMenuTwice() throws IOException {
		expectInitialMenu();
		expect(mockReader.readLine()).andReturn("1").times(2);
		mockOut.println(startsWith("1: Main menu (this)\n"));
		expectLastCall().times(2); 
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}
	
	private void expectInitialMenu() {
		mockOut.println(endsWith("Exit\n"));
		expectLastCall();
	}
	
	private void expectUserExits() throws IOException {
		expect(mockReader.readLine()).andReturn("4");
		mockOut.println(startsWith("Thank you"));
		expectLastCall();
	}

}
