package menusystem.part1;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.Before;
import org.junit.Test;

// Initial test: string content of main menu
public class DemoTest {

	private Demo demo;
	
	@Before
	public void setUp() throws Exception {
		demo = new Demo();
	}

	@Test
	public void mainMenuContent() {
		assertThat(demo.mainMenu(), startsWith("1: Main menu (this)\n"));
		assertThat(demo.mainMenu(), endsWith("4: Exit\n"));
	}
}
