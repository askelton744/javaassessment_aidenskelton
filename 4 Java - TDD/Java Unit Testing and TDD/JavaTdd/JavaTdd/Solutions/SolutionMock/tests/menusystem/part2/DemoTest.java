package menusystem.part2;

import static org.easymock.EasyMock.endsWith;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertThat;

import java.io.PrintStream;

import org.easymock.EasyMockSupport;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

// Initial specification of void run() method; mock System.out
public class DemoTest extends EasyMockSupport {

	private Demo demo;
	private PrintStream mockOut;

	@Before
	public void setUp() throws Exception {
		mockOut = createMock(PrintStream.class);
		demo = new Demo(mockOut);
	}

	@Test
	public void mainMenuContent() {
		assertThat(demo.mainMenu(), Matchers.startsWith("1: Main menu (this)\n"));
		assertThat(demo.mainMenu(), Matchers.endsWith("4: Exit\n"));
	}

	@Test
	public void run_prints_mainMenu_toConsole() {
		mockOut.println(endsWith("Exit\n"));
		expectLastCall();
		replayAll();
		demo.run();
		verifyAll();
	}
}
