package menusystem.part6;

import static org.easymock.EasyMock.endsWith;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.startsWith;
import static org.junit.Assert.assertThat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import org.easymock.EasyMockSupport;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

// Test to specify that run() gracefully handles unrecognised input 
public class DemoTest extends EasyMockSupport {

	private Demo demo;
	private PrintStream mockOut;
	private BufferedReader mockReader; 

	@Before
	public void setUp() throws Exception {
		mockOut = createNiceMock(PrintStream.class);
		mockReader = createNiceMock(BufferedReader.class);
		demo = new Demo(mockOut, mockReader);
	}

	@Test
	public void mainMenuContent() {
		assertThat(demo.mainMenu(), Matchers.startsWith("1: Main menu (this)\n"));
		assertThat(demo.mainMenu(), Matchers.endsWith("4: Exit\n"));
	}

	@Test
	public void userExitsDemoImmediately() throws IOException {
		expectInitialMenu();
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}

	@Test
	public void readAnIntReturnsValidInt() throws IOException {
		expect(mockReader.readLine()).andReturn("1");
		replayAll();
		
		int entered = demo.readAnInt();
		verifyAll();
		assertThat(entered, Matchers.is(1));
	}
	
	@Test
	public void readAnIntGivenInvalidReturnsMinus1() throws IOException {
		expect(mockReader.readLine()).andReturn("");
		replayAll();
		
		int entered = demo.readAnInt();
		verifyAll();
		assertThat(entered, Matchers.is(-1));
	}

	@Test
	public void userRequestsMainMenu() throws IOException {
		expectInitialMenu();
		expect(mockReader.readLine()).andReturn("1");
		mockOut.println(startsWith("1: Main menu (this)\n"));
		expectLastCall(); 
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}

	@Test
	public void userRequestsMainMenuTwice() throws IOException {
		expectInitialMenu();
		expect(mockReader.readLine()).andReturn("1").times(2);
		mockOut.println(startsWith("1: Main menu (this)\n"));
		expectLastCall().times(2); 
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}
	
	@Test 
	public void badUserInputIsCaught() throws IOException {
		expectInitialMenu(); 
		expect(mockReader.readLine()).andReturn("7");
		mockOut.println(startsWith("Sorry, unrecognised input"));
		expectLastCall(); 
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}
	
	private void expectInitialMenu() {
		mockOut.println("Please enter a number:");
		expectLastCall(); 
		mockOut.println(endsWith("Exit\n"));
		expectLastCall();
	}
	
	private void expectUserExits() throws IOException {
		expect(mockReader.readLine()).andReturn("4");
		mockOut.println(startsWith("Thank you"));
		expectLastCall();
	}
}
