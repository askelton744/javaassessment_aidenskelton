package solution.junit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DateTest {

	Date promotion;

	// Setup - create an object we can use in our tests. 
	@Before
	public void setUp() {
		promotion = new Date(30, 5, 2011);	// Swansea City win promotion to Premier League :)
	}

	// Tests for setters.
	@Test(expected=IllegalArgumentException.class)
	public void setDay_InvalidDay_Throws() {
		promotion.setDay(32);
	}

	@Test(expected=IllegalArgumentException.class)
	public void setMonth_InvalidMonth_Throws() {
		promotion.setMonth(0);
	}

	// Tests for dateByDays().
	@Test
	public void dateByDays_ZeroDays_ReturnsSameDay() {
		// Change the arg to 1 to to see the failure message: 
		Date actual = Date.dateByDays(promotion, 0);
		assertEquals("Same day", promotion, actual);
	}
	
	@Test
	public void dateByDays_Plus1Day_ReturnsNextDay() {
		Date actual = Date.dateByDays(promotion, 1);
		assertEquals("Next day", new Date(31, 5, 2011), actual);
	}
	
	@Test
	public void dateByDays_Minus1Day_ReturnsPreviousDay() {
		// To make it absolutely clear in the assert, it's sometimes useful to create an explicit variable for the expected result.
		Date expected = new Date(29, 5, 2011); 
		Date actual = Date.dateByDays(promotion, -1);
		assertEquals("Previous day", expected, actual);
	}
	
	@Test
	public void dateByDays_Plus30Days_RollsToNewMonth() {
		Date expected = new Date(29, 6, 2011);
		Date actual = Date.dateByDays(promotion, 30);
		assertEquals("Next month", expected, actual);
	}
	
	@Test
	public void dateByDays_Plus10Months_RollsToYearAfter() {
		Date expected = new Date(30, 3, 2012);
		Date actual = Date.dateByDays(promotion, 310);   // About 10 months.
		assertEquals("Next year", expected, actual);
	}
	
	@Test
	public void dateByDays_Minus10Months_RollsToYearBefore() {
		Date expected = new Date(30, 7, 2010);
		Date actual = Date.dateByDays(promotion, -310);   // About 10 months in the past.
		assertEquals("Previous year", expected, actual);
	}
	
	@Test
	public void dateByDays_NewYearsEvePlus1Day_ReturnsJan1stNextYear() {
		Date expected = new Date(1, 1, 2014);
		Date newYearsEve = new Date(31, 12, 2013);
		Date actual = Date.dateByDays(newYearsEve, 1);
		assertEquals("New Year's Day", expected, actual);
	}
	
	// Tests for equals() and hashCode().
	@Test
	public void equals_NullDate_ReturnsFalse() {
		assertFalse(promotion.equals(null));
	}
	
	@Test
	public void equals_NonDateObject_ReturnsFalse() {
		assertFalse(promotion.equals("30/5/2011"));
	}

	@Test
	public void equals_DifferentDate_ReturnsFalse() {
		assertFalse(promotion.equals(new Date(3, 12, 1964)));
	}

	@Test
	public void equals_SameDate_ReturnsTrue() {
		assertTrue(promotion.equals(new Date(30, 5, 2011)));
	}

	@Test
	public void equalsDates_HaveSamehashCode() {
		Date promotion2 = new Date(30, 5, 2011);
		// guard assertion pattern: 
		assertEquals(promotion, promotion2);
		// normally not good to have 2 asserts in same test, but here
		// it makes sense: we want to assert that the hashCodes of 
		// equals() objects are the same.  
		assertEquals(promotion.hashCode(), promotion2.hashCode());
	}
	
}
