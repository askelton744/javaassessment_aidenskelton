use training;

 drop table if exists payment;
drop table  if exists  customer;


CREATE TABLE `customer` (
  `customer_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



CREATE TABLE `payment` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `payment_date` date NOT NULL,
  `customer_id` smallint(5) unsigned NOT NULL,
  `payment_type` varchar(45) ,
  `amount` decimal(5,2) NOT NULL,
  PRIMARY KEY (`payment_id`),
  CONSTRAINT `fk_payment_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON UPDATE CASCADE
);
  

Insert into customer (first_name, last_name, email)
values ('PATRICIA'	,'JOHNSON',	'PATRICIA.JOHNSON@sakilacustomer.org');
Insert into customer (first_name, last_name, email)
values ('LINDA'	,'WILLIAMS',	'LINDA.WILLIAMS@sakilacustomer.org');
Insert into customer (first_name, last_name, email)
values ('BARBARA'	,'JONES',	'LINDA.WILLIAMS@sakilacustomer.org');
Insert into customer (first_name, last_name, email)
values ('ELIZABETH'	,'BROWN',	'ELIZABETH.BROWN@sakilacustomer.org');
Insert into customer (first_name, last_name, email)
values ('JENNIFER'	,'DAVIS',	'ELIZABETH.BROWN@sakilacustomer.org');
Insert into customer (first_name, last_name, email)
values ('MARIA'	,'MILLER',	'MARIA.MILLER@sakilacustomer.org');


INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-08-28',2,'Visa',20.89);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-08-27',1,'MasterCard',80.50);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-08-12',3,'Visa',100.89);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-07-28',1,'Visa',20.66);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-08-03',1,'Visa',33.89);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-06-01',1,'MasterCard',20.33);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-06-11',1,'Visa',100.00);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-06-27',1,'MasterCard',80.50);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-06-13',3,'Visa',200.00);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-06-28',5,'Visa',20.66);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-06-11',1,'Visa',300.00);
INSERT INTO payment(payment_date,customer_id,payment_type,amount)
VALUES ('2018-06-01',1,'MasterCard',20.33);




Select * from Customer;
Select * from Payment;