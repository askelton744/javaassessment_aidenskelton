package com.citi.paymentapp.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.paymentapp.business.PaymentBs;
import com.citi.paymentapp.dto.Payment;


@RestController
@RequestMapping("/payment")
public class PaymentController {
	@Autowired
	PaymentBs paymentBs;
	
	@RequestMapping(value= "/status",method=RequestMethod.GET)
	public String getStatus() {
		return "Payment Controller Running";
	}
	
	@RequestMapping(value= "/findById",method=RequestMethod.GET)
	public List<Payment> findById(String id) {
		return paymentBs.findById(id);
	}
	
	

}
