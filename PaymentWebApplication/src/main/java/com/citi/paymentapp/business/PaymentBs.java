package com.citi.paymentapp.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.paymentapp.dao.PaymentDao;
import com.citi.paymentapp.dto.Payment;

@Component
public class PaymentBs {
	@Autowired
	PaymentDao paymentDao;

	public int rowCount() {
		return paymentDao.rowCount();
	}

	public List<Payment> findById(String id) {
		if (id.length() < 0) {
			throw new IllegalArgumentException();
		} else {
			return paymentDao.findById(id);
		}
	}

	public List<Payment> findByType(String type) {
		if (type.isEmpty()) {
			throw new IllegalArgumentException();
		}
		if (type.equals("")) {
			throw new IllegalArgumentException();
		} else {

			return null;

		}
	}

	public int save(Payment payment) {
		// TODO Auto-generated method stub
		return 0;
	}
}
