package com.citi.paymentapp.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.paymentapp.dto.Payment;

@Component
public class PaymentDao {
	@Autowired
	private JdbcTemplate tpl;
	public int rowCount() {
		return tpl.queryForObject//where you insert select statements
				("SELECT COUNT(*) FROM payment",
				Integer.class);
	
	}
	public List<Payment> findByType(String type) {
		return tpl.query(
				"SELECT * FROM payment WHERE payment_type=?",
				new Object[] {type},
				new PaymentMapper());
				
	}
	
	public List<Payment> findById(String id) {
		return tpl.query(
				"SELECT * FROM payment WHERE payment_id=?",
				new Object[] {id},
				new PaymentMapper());
	}
	
	public int save (Payment payment) {
		
		 KeyHolder keyHolder = new GeneratedKeyHolder();
	        this.tpl.update(
	            new PreparedStatementCreator() {	   
	                @Override
	                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	                    PreparedStatement ps =
	                            connection.prepareStatement("insert into payment"
	                            		+ "(payment_id, payment_date,payment_type,payment_amount"
	                            		+ "customer_id(?, ?, ?,?,?)",Statement.RETURN_GENERATED_KEYS);
	                    ps.setInt(1, payment.getId());
	                    ps.setDate(2, (Date) payment.getPaymentDate());
	                    ps.setString(3, payment.getType());
	                    ps.setDouble(4, payment.getAmount());
	                    ps.setInt(5, payment.getCustId());
	                  
	                    return ps;
	                }
	            },
	            keyHolder);
	        return 0;

	}
	
	private static final class PaymentMapper implements RowMapper<Payment> {
	    public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
	        Payment payment = new Payment(
	        		rs.getInt("payment_id"),
	        		rs.getDate("payment_date"),
	        		rs.getString("payment_type"),
	        		rs.getDouble("amount"),
	        		rs.getInt("customer_id")
	        		);
	       
	        
			return payment;
	    }//maps the data output from the database into the constructor
	}//private class only used within CustomerDao 
	
}


//public int save (Customer customer) {
//	
//	 KeyHolder keyHolder = new GeneratedKeyHolder();
//       this.tpl.update(
//           new PreparedStatementCreator() {	   
//               @Override
//               public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
//                   PreparedStatement ps =
//                           connection.prepareStatement("insert into customers(CustomerId, CompanyName, ContactName, Address, City, Country) "
//                           		+ "values (?, ?, ?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
//                   ps.setString(1, customer.getCustomerId());
//                   ps.setString(2, customer.getCompanyName());
//                   ps.setString(3, customer.getContactName());
//                   ps.setString(4, customer.getAddress());
//                   ps.setString(5, customer.getCity());
//                   ps.setString(6, customer.getCountry());
//                   
//              
//                   return ps;
//               }
//           },
//           keyHolder);
//       return 0;
//
//}
