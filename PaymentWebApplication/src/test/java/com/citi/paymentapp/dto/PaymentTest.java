package com.citi.paymentapp.dto;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.paymentapp.dto.Payment;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentTest {
	Date date=new Date();
	
	
	@Test
	public void product_CanInstantiate_ReturnNotNull() {
		assertThat(new Payment(25,date,"Visa",75.78,9),
				is(not(nullValue())));
	}//check that new products can be added


}
