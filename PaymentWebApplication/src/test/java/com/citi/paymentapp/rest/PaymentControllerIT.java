package com.citi.paymentapp.rest;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentControllerIT {
	RestTemplate restTemplate=new RestTemplate();
	@Test
	public void productController_CheckStatus_ReturnsRunning() {
		String url=
				"http://localhost:8082/products/status";
		ResponseEntity<String> response=
				restTemplate.getForEntity(url, String.class);
		assertTrue(response.getBody().equals("Product Controller Running"));
		
		//fail("Not yet implemented");
	}

}
