package com.citi.paymentapp.bs;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.paymentapp.business.PaymentBs;
import com.citi.paymentapp.dto.Payment;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentAppBsIT {
@Autowired
	PaymentBs paymentBs;
	@Test
	public void paymentBs_checkRowCount_returnsOver0() {
		paymentBs.rowCount();
		assertTrue(paymentBs.rowCount()<=81);	
	}//tests that something has been read from the database
	
	@Test
	public void paymentBs_findViewById_returnsView() {
		paymentBs.findById("?");
	}
	
	@Test
	public void paymentBs_findViewbyType_returnsView() {
		paymentBs.findByType("Visa");
	}
	
	
	@Test
	public void paymentBs_saveCustomer_ReturnCustomer() {
		int result=paymentBs.save(new Payment());
		List<Payment> payment=paymentBs.findById("1");
		assertTrue(payment.size()>0);
	}
}
