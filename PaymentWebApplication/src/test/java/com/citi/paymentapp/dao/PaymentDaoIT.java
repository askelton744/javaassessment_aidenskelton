package com.citi.paymentapp.dao;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.paymentapp.dto.Payment;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentDaoIT {
	@Autowired
	PaymentDao paymentDao;
	Date date=new Date();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void paymentDao_checkRowCount_returnsOver0() {
		assertTrue(paymentDao.rowCount()>0);	
	}//tests that something has been read from the database
	
	@Test
	public void paymentDao_saveCustomer_ReturnPayment() {
		int res=paymentDao.save(new Payment(12,date,"Visa",25.20,3));
		//Payment payment=paymentDao.findById("12");
		//assertTrue(payment.getType().equals("Visa"));
	}
	
	@Test//--Causing Null Pointer
	public void paymentDao_checkFindById_returnsId() {
		//TODO general test
		List<Payment> payment = paymentDao.findById("1");
		assertTrue(payment.size()==1);
	}//tests that findById finds the correct customer
	
	@Test
	public void paymentDao_checkFindByType_returnsList() {
		List<Payment> payment=paymentDao.findByType("Visa");
		assertTrue(payment.size()==8);
	}

}
