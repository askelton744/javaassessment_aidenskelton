package com.citi.pollingengine;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class ScheduleTask {
	@Scheduled(fixedDelay = 5000)
	public void scheduleFixedDelayTask() {
	    System.out.println(
	      "Fixed delay task - " + System.currentTimeMillis() / 1000);
	   OnlineFeed onlinefeed=new OnlineFeed();
	   onlinefeed.fetchToDo();
	}
}
