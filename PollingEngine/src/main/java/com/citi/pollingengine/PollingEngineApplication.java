package com.citi.pollingengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PollingEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(PollingEngineApplication.class, args);
	}
}
