package demo.additionaltechniques;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {

		ApplicationContext ctx = SpringApplication.run(Application.class, args);
        
		// Uncomment statements in this code as appropriate, to explore each piece at a time.
		
		MyBean1 bean1 = ctx.getBean(MyBean1.class);
        System.out.println(bean1);
        
		// MyBean2a bean2a = ctx.getBean(MyBean2a.class);
		// System.out.println(bean2a);
		
		// MyBean2b bean2b = ctx.getBean(MyBean2b.class);
		// System.out.println(bean2b);
		
		// MyBean3 bean3 = ctx.getBean(MyBean3.class);
		// System.out.println(bean3);

		// MyBean4 bean4 = ctx.getBean(MyBean4.class);
		// System.out.println(bean4);
	}
}
