package demo.additionaltechniques;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Lazy
@Profile("production")
public class MyBean3Prod implements MyBean3 {

	@Override
	public String toString() {
		return "Hello from MyBean3Prod";
	}
}