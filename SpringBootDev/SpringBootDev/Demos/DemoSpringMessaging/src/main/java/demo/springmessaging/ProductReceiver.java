package demo.springmessaging;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
@Component
public class ProductReceiver {
	@JmsListener(destination="ProductTickerAlertDest",containerFactory="myFactory")
	public void receiveProductTickerAlertMessafe(Product message) {
		System.out.print("Product ticker alert message received: "+message);
	}
	
}

