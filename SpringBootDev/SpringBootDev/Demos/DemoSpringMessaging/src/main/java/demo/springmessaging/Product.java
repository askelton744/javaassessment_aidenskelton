package demo.springmessaging;

public class Product {
	private String productName;
	private String productType;
	private double productCost;
	
	public Product() {
		
	}
	public Product(String productName, String productType, double productCost) {
		super();
		this.productName = productName;
		this.productType = productType;
		this.productCost = productCost;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public double getProductCost() {
		return productCost;
	}
	public void setProductCost(double productCost) {
		this.productCost = productCost;
	}

	

}
