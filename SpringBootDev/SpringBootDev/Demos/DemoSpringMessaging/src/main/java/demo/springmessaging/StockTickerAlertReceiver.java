package demo.springmessaging;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class StockTickerAlertReceiver {

	@JmsListener(destination="stockTickerAlertDest", containerFactory="myFactory")
	public void receiveStockTickerAlertMessage(StockTickerAlert message) {
		System.out.println("Stock ticker alert message received: " + message);
	}
}
