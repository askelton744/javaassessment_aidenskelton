package demo.springmessaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
@Component
public class ProductSender {
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public void sendProductTickerAlertMessage(String message) {
		System.out.print("\nSending: "+message);
		jmsTemplate.convertAndSend("productTickerAlertDes",message);
		
		try {
			Thread.sleep(300);
		}
		catch(Exception ex) {
			
		}
	}
	

//	@Component
//	public class StockTickerAlertSender {
//
//		@Autowired
//		private JmsTemplate jmsTemplate;
//
//		public void sendStockTickerAlertMessage(StockTickerAlert message) {
//			System.out.println("Sending: " + message);
//	        jmsTemplate.convertAndSend("stockTickerAlertDest", message);	
//	        
//	        try {
//	        	Thread.sleep(300); 
//	        } 
//	        catch (Exception ex) {}
//	    }
}
