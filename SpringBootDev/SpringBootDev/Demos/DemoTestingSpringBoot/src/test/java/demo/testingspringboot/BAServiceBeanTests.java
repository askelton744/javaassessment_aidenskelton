package demo.testingspringboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BAServiceBeanTests {

    @Autowired
    BAServiceBean service;

    @MockBean
    private BARepository mockRepos;

    @Test
    public void testDeposit() {

        BankAccountBean acc = new BankAccountBean();
        when(mockRepos.getById(anyInt())).thenReturn(acc);

        service.depositIntoAccount(1234, 100);
        assertEquals(acc.getBalance(), 100);

        verify(mockRepos).getById(anyInt());
        verify(mockRepos).update(anyInt(), refEq(acc));
    }
}
