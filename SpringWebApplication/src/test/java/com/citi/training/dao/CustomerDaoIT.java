package com.citi.training.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.dto.Customer;
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerDaoIT {

	@Autowired
	CustomerDao customerDao;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void customerDao_CheckRowCount_ReturnsOver0() {
	
		assertTrue(customerDao.rowCount()>0);
		
	}

	@Test
	public void customerDao_CheckFindById_ReturnsContact() {
	//todo general test
		Customer customer=customerDao.findbyId("ANTON");
		assertTrue(customer.getContactname().
				equals("Antonio Moreno"));
		
	}
	
	@Test
	public void customerDao_DeleteCustomer() {
	//todo general test
		customerDao.save(new Customer("Z0001","Z Ltd", "Bob",
				"Belfast","Antrim", "Ni"));
		customerDao.delete("Z0001");

		Customer customer = customerDao.findbyId("Z0001");		
	
			
		assertTrue(customer==null);
		
	}
	
	@Test
	public void customerDao_CheckFindAll_ReturnsAllRecords() {
		//todo general test
			List<Customer> customers=customerDao.findAll();
			
			assertTrue(customers.size()==	
					customerDao.rowCount());
			
		}

	
	
	@Test
	public void customerDao_SaveCustomer_ReturnsCustomer() {
		//todo general test
			int result=customerDao.save(new Customer("B0001","X Ltd", "Bob",
					"Belfast","Antrim", "Ni"));
			Customer customer = customerDao.findbyId("B0001");		
			assertTrue(customer.getContactname().equals("Bob"));	
			//delete record after
		}
	
	
	@Test
	public void customerDao_UpdateCustomer_ReturnsCustomer() {
		//todo general test
		customerDao.update(new Customer("BERGS","X Ltd", "Bob",
					"Main St","Belfast", "Ni"));
			Customer customer = customerDao.findbyId("BERGS");		
			assertTrue(customer.getContactname().equals("Bob"));
			assertTrue(customer.getCity().equals("Belfast"));	

			//delete record after
		}
	
	
	
	
	
}
