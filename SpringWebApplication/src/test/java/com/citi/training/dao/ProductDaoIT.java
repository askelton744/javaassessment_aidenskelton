package com.citi.training.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.dto.Customer;
import com.citi.training.dto.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductDaoIT {
	@Autowired
	ProductDao productDao;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void productDao_CheckRowCount_ReturnsOver0() {
	
		assertTrue(productDao.rowCount()>0);
		
	}

	@Test
	public void productDao_CheckFindById_ReturnsContact() {
		Product product=productDao.findbyId(1);
		assertTrue(product.getProductName().
				equals("Chai"));
		
	}
	
	@Test
	public void customerDao_CheckFindAll_ReturnsAllRecords() {
			List<Product> products=productDao.findAll();
			
			assertTrue(products.size()==	
					productDao.rowCount());
			
		}

	
	
	@Test
	public void customerDao_SaveCustomer_ReturnsCustomer() {
			int result=productDao.save(new Product(1,"Pencil",1,1,"1 per pack",20.00,30,40,1,1));
			Product product = productDao.findbyId(result);		
			assertTrue(product.getProductName().equals("Pencil"));	
			//delete record after
		}
	
	
	@Test
	public void customerDao_UpdateCustomer_ReturnsCustomer() {
		//todo general test
		
		
		Product product=productDao.update(new Product(1,"Chai",1,1,"1 per pack",20.29,30,40,1,1));

			 product = productDao.findbyId(1);		
			assertTrue(product.getProductName().equals("Chai"));
			assertTrue(product.getUnitPrice()==20.29);
	
		
			//delete record after
		}
	
}
