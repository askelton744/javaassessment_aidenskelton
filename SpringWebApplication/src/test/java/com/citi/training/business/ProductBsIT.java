package com.citi.training.business;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.dto.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductBsIT {

	@Autowired
	ProductBs productBs;
	
	@Test(expected=IllegalArgumentException.class)
	public void productBs_InvalidIdLength_ReturnsIllegalArgExc() {
		productBs.findbyId(0);
	}
	
	@Test()
	public void productBs_ValidIdLength_Returnsproduct() {
			Product product=productBs.findbyId(1);
			assertThat(product, is(not(nullValue())));		
	}

	@Test()
	public void ProductBs_ListAllproducts_ReturnsListproduct() {
			List<Product> products=productBs.findAll();
			assertTrue(products.size()== productBs.rowCount());		
	}
	@Test
	public void productBs_SaveProduct_ReturnsProduct() {
		//todo general test
		Product product=productBs.update(new Product(1,"Chai",1,1,"1 per pack",20.29,300,40,1,1));
			 product = productBs.findbyId(1);		
			assertTrue(product.getUnitsInStock()==300);	
			//delete record after
		}
	
	
}
