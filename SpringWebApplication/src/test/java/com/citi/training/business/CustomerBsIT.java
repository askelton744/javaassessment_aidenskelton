package com.citi.training.business;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.dto.Customer;
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerBsIT {

	@Autowired
	CustomerBs customerBs;
	
	@Test(expected=IllegalArgumentException.class)
	public void CustomerBs_InvalidIdLength_ReturnsIllegalArgExc() {
			customerBs.findbyId("Anto");
	}
	
	@Test()
	public void CustomerBs_ValidIdLength_ReturnsCustomer() {
			Customer customer=customerBs.findbyId("Anton");
			assertThat(customer, is(not(nullValue())));		
	}

	@Test()
	public void CustomerBs_ListAllCustomers_ReturnsListCustomer() {
			List<Customer> customers=customerBs.findAll();
			assertTrue(customers.size()== customerBs.rowCount());		
	}
	@Test
	public void customerBs_SaveCustomer_ReturnsCustomer() {
		//todo general test
			int result=customerBs.save(new Customer("S0001","X Ltd", "Bob",
					"Belfast","Antrim", "Ni"));
			Customer customer = customerBs.findbyId("S0001");		
			assertTrue(customer.getContactname().equals("Bob"));	
			//delete record after
		}
	
	
}
