package com.citi.training.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductRestIT {

	
	
	RestTemplate restTemplate = new RestTemplate() ;
	
	@Test
	public void ProductController_checkStatus_returnsrunning()  
	{
		///todo property
		String url  = "http://localhost:8082/products/status";
		ResponseEntity<String> response
		  = restTemplate.getForEntity(url , String.class);
		
	 assertTrue(response.getBody().
			 equals("Product Controller running"));
	}

}
