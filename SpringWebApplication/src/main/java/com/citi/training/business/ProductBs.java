package com.citi.training.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.dao.ProductDao;

import com.citi.training.dto.Product;

@Component
public class ProductBs {

	@Autowired
	ProductDao productDao;

	public int rowCount()
	{
		return productDao.rowCount();
		
	}
	
	public Product update(Product product) {
		if (product.getProductName().equals(""))
		{
			throw new IllegalArgumentException();

		}
		return productDao.update(product);
		
	}
	
	public void delete(int id) {
		if (id <= 0)
		{
			throw new IllegalArgumentException();

		}
		
		productDao.delete(id);
			
	
		
	}
	
	
    public Product findbyId (int id) {
    	if (id<=0)
    	{
    		throw new IllegalArgumentException();
    		
    	} 	
    	return productDao.findbyId(id)	; 	
    }
    
    
    public List<Product> findAll () {
    	   
    	return productDao.findAll();
    		
    	}
    
    public int save(Product product) {
    	
    	if (product.getProductName()==null )
    	{
    		throw new IllegalArgumentException();
    	}
    	
    	
    	return productDao.save(product);
    	
    	
    }
	
}

