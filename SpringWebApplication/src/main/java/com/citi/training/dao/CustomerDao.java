package com.citi.training.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.dto.Customer;


//First, and most important - all Spring beans are managed - 
//they "live" inside a container, called "application context".

//What is "living" in the application context? This means that the
// context instantiates the objects, not you. I.e. - you never
//make new UserServiceImpl() -
//the container finds each injection point and sets an 
//instance there.

@Component  
public class CustomerDao {
	 private static final Logger logger = LogManager.getLogger(CustomerDao.class);

	// Tells the application context to inject an instance
	//of JdbcTemplate here
	   @Autowired
	    private JdbcTemplate tpl;
	    
	   public Customer update(Customer customer)
	   {
		   tpl.update("Update Customers  "
		   		+ "Set CompanyName=?, ContactName=?," + 
		   		" Address=?, City=?, Country=?" + 
		   		" Where CustomerID=?" ,
		   		customer.getName(),
		   		customer.getContactname(),
		   		customer.getAddress(),
		   		customer.getCity(),
		   		customer.getCountry(),
		   		customer.getCustomerid()
				   
				   );
		   return customer;
	   }
	   
	   
	   public void delete(String id)
	   {
		   tpl.update("delete from Customers  "
		   		+ " Where CustomerID=?" ,
		   		id		  );  
	   }

	    public int rowCount() {
	        return tpl.queryForObject
	        		("select count(*) from customers",
	        				Integer.class);
	    }

	
	    //Select * from customers where customerid='Anton'
	    
	    public Customer findbyId (String id) {
	        return tpl.queryForObject
	        		("Select * from customers "
	        				+ "where customerid=?", 
	        				new Object[]{id},
	        				new CustomerMapper());
	    }

	    public List<Customer> findAll () {
	    	logger.info("Find all invoked");
	        return tpl.query("Select * from customers",
	        		new CustomerMapper());
	    }
	    
	    
	  //  Insert into Customers
	   // (CustomerID, Companyname,ContactName, Address, City, Country)
	   // values ('D0001', 'D Co', 'Dee', 'Clare','Clare','ireland');

	    
	    public int save (Customer customer)
	    {
	    	KeyHolder keyHolder = new GeneratedKeyHolder();
	    //	https://www.hastebin.com/keqapikide.coffeescript
	    //	https://www.hastebin.com/fetojiwoga.css
	    	 this.tpl.update(new PreparedStatementCreator() {
	 	                @Override
	 	                public PreparedStatement createPreparedStatement
	 	                (Connection connection) throws SQLException {
	 	                  
	 	                	PreparedStatement ps =
	 	                            connection.prepareStatement
("insert into customers(CustomerID, Companyname,ContactName, Address, City, Country)"
		+ " values (?, ?,?,?,?,?)" ,
	 	                     Statement.RETURN_GENERATED_KEYS);
	 	                    ps.setString(1, customer.getCustomerid());
	 	                    ps.setString(2, customer.getName());         
	 	                    ps.setString(3, customer.getContactname());
	 	                    ps.setString(4, customer.getAddress());
	 	                    ps.setString(5, customer.getCity());
	 	                    ps.setString(6, customer.getCountry());
	 	               
	 	                   return ps;
		                }
		            },
		            keyHolder);
						
	 	    //    return keyHolder.getKey().intValue();
	    	return 0;

	    }
	    

	    private static final class CustomerMapper 
	    implements RowMapper<Customer> {
	        public Customer mapRow(ResultSet rs, int rowNum)
	        		throws SQLException {
	            Customer customer = new Customer
	            (rs.getString("customerid")	,
	            rs.getString("companyname"),
	            rs.getString("contactname"),
	            rs.getString("address"),
	            rs.getString("city"),
	            rs.getString("country"));	            	            
	    		return customer;
	        }	    
}
}