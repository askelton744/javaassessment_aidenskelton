package com.citi.training.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.dto.Product;
@Component
public class ProductDao {
	 private static final Logger logger = LogManager.getLogger(CustomerDao.class);

	// Tells the application context to inject an instance
	//of JdbcTemplate here
	   @Autowired
	    private JdbcTemplate tpl;
	    
	   public Product update(Product product)
	   {
		   tpl.update("UPDATE products " +
		   		 " SET " + 
		   		"  ProductName= ?" +
		   		" ,SupplierID= ?" + 
		   		" ,CategoryID= ?" + 
		   		" ,QuantityPerUnit= ?" + 
		   		" ,UnitPrice= ?" +  
		   		" ,UnitsInStock= ?" + 
		   		" ,UnitsOnOrder= ?" +  
		   		" ,ReorderLevel= ?" + 
		   		" ,Discontinued= ?" +  
		   		" WHERE ProductID = ?" ,
		   		product.getProductName(),
		   		product.getSupplierID(),
		   		product.getCategoryID(),
		   		product.getQuantityPerUnit(),
		   		product.getUnitPrice(),
		   		product.getUnitsInStock(),
		   		product.getUnitsOnOrder(),
		   		product.getReorderLevel(),
		   		product.getDiscontinued(),
		   		product.getProductid()
   		
				   );
		   return product;
	   }
	   
	   
	   public void delete(int id)
	   {
		   tpl.update("delete from products  "
		   		+ " Where productID=?" ,
		   		id		  );  
	   }

	    public int rowCount() {
	        return tpl.queryForObject
	        		("select count(*) from products",
	        				Integer.class);
	    }

	
	    
	    public Product findbyId (int id) {
	        return tpl.queryForObject
	        		("Select * from products "
	        				+ "where productid=?", 
	        				new Object[]{id},
	        				new ProductMapper());
	    }

	    public List<Product> findAll () {
	    	logger.info("Find all invoked");
	        return tpl.query("Select * from Products",
	        		new ProductMapper());
	    }
	    
	    public int save (Product product)
	    {
	    	KeyHolder keyHolder = new GeneratedKeyHolder();
	    	 this.tpl.update(new PreparedStatementCreator() {
	 	                @Override
	 	                public PreparedStatement createPreparedStatement
	 	                (Connection connection) throws SQLException {
	 	                  
	 	                	PreparedStatement ps =
	 	                            connection.prepareStatement
("insert into products(  ProductName , SupplierID , CategoryID , QuantityPerUnit , UnitPrice , UnitsInStock , UnitsOnOrder , ReorderLevel ,  Discontinued )"
		+ " values (?, ?,?,?,?,?,?,?,?)" ,
	 	                     Statement.RETURN_GENERATED_KEYS);
	 	                    ps.setString(1, product.getProductName());
	 	                    ps.setInt(2, product.getSupplierID());        
	 	                    ps.setInt(3, product.getCategoryID());
	 	                    ps.setString(4, product.getQuantityPerUnit());
	 	                    ps.setDouble(5, product.getUnitPrice());
	 	                    ps.setInt(6, product.getUnitsInStock());
	 	                    ps.setInt(7, product.getUnitsOnOrder());
	 	                    ps.setInt(8, product.getUnitsOnOrder());
	 	                    ps.setInt(9, product.getDiscontinued());
 	               
	 	                   return ps;
		                }
		            },
		            keyHolder);
						
	 	       return keyHolder.getKey().intValue();
	    	

	    }
	    

	    private static final class ProductMapper 
	    implements RowMapper<Product> {
	        public Product mapRow(ResultSet rs, int rowNum)
	       		throws SQLException {
	
	        	
	        	Product product = new Product
	            (rs.getInt("Productid")	,
	            rs.getString("ProductName"),
	            rs.getInt("SupplierID"),
	            rs.getInt("CategoryID"),
	            rs.getString("QuantityPerUnit"),
	            rs.getDouble("UnitPrice"),
	            rs.getInt("UnitsInStock"),
	            rs.getInt("UnitsOnOrder"),
	            rs.getInt("ReOrderLevel"),
	            rs.getInt("Discontinued")            		
	            		);	            	            
	    		return product;
	        }	    
}
}
