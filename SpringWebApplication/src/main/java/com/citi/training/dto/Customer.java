package com.citi.training.dto;

public class Customer extends DbMetadata {
	private String customerid;
	private String name;
	private String contactname;
	private String address;
	private String city;
	private String country;
	
	public Customer()
	{}
	
	public Customer(String customerid, String name, String contactname, String address, String city, String country) {
		
		this.customerid = customerid;
		this.name = name;
		this.contactname = contactname;
		this.address = address;
		this.city = city;
		this.country = country;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContactname() {
		return contactname;
	}
	public void setContactname(String contactname) {
		this.contactname = contactname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
	
	
	
	
}
