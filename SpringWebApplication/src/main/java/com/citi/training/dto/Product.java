package com.citi.training.dto;

public class Product {

	private int productid;
	private String  ProductName;
	private int  SupplierID;
	private int  CategoryID;
	private String  QuantityPerUnit;
	private double  UnitPrice;
	private int  UnitsInStock;
	private int UnitsOnOrder;
	private int  ReorderLevel;
	private int Discontinued;	
	
	
	public Product(){}
	
	
	
	public Product(int productid, String productName, int supplierID, int categoryID, String quantityPerUnit,
			double unitPrice, int unitsInStock, int unitsOnOrder, int reorderLevel, int discontinued) {
		super();
		this.productid = productid;
		ProductName = productName;
		SupplierID = supplierID;
		CategoryID = categoryID;
		QuantityPerUnit = quantityPerUnit;
		UnitPrice = unitPrice;
		UnitsInStock = unitsInStock;
		UnitsOnOrder = unitsOnOrder;
		ReorderLevel = reorderLevel;
		Discontinued = discontinued;
	}



	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public int getSupplierID() {
		return SupplierID;
	}
	public void setSupplierID(int supplierID) {
		SupplierID = supplierID;
	}
	public int getCategoryID() {
		return CategoryID;
	}
	public void setCategoryID(int categoryID) {
		CategoryID = categoryID;
	}
	public String getQuantityPerUnit() {
		return QuantityPerUnit;
	}
	public void setQuantityPerUnit(String quantityPerUnit) {
		QuantityPerUnit = quantityPerUnit;
	}
	public double getUnitPrice() {
		return UnitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		UnitPrice = unitPrice;
	}
	public int getUnitsInStock() {
		return UnitsInStock;
	}
	public void setUnitsInStock(int unitsInStock) {
		UnitsInStock = unitsInStock;
	}
	public int getUnitsOnOrder() {
		return UnitsOnOrder;
	}
	public void setUnitsOnOrder(int unitsOnOrder) {
		UnitsOnOrder = unitsOnOrder;
	}
	public int getReorderLevel() {
		return ReorderLevel;
	}
	public void setReorderLevel(int reorderLevel) {
		ReorderLevel = reorderLevel;
	}
	public int getDiscontinued() {
		return Discontinued;
	}
	public void setDiscontinued(int discontinued) {
		Discontinued = discontinued;
	}
	
}
