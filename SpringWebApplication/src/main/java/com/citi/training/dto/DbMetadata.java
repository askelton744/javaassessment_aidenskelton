package com.citi.training.dto;

import java.util.Date;

public class DbMetadata {
private int id;
private String modifiedby;
private Date datemodifiedby;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getModifiedby() {
	return modifiedby;
}
public void setModifiedby(String modifiedby) {
	this.modifiedby = modifiedby;
}
public Date getDatemodifiedby() {
	return datemodifiedby;
}
public void setDatemodifiedby(Date datemodifiedby) {
	this.datemodifiedby = datemodifiedby;
}



}
