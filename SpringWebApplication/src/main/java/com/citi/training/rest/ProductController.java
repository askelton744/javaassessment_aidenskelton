package com.citi.training.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.business.ProductBs;
import com.citi.training.dto.Product;

@RestController
@RequestMapping("/products")
public class ProductController {
	 private static final Logger logger = LogManager.getLogger(ProductController.class);
	@Autowired
	ProductBs productBs;
	
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
public Product update(@RequestBody Product product)
{

		return productBs.update(product);
		
}
	
	
	
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public String getStatus()
	{
		logger.info("Product controller started");
		
		return "Product Controller running";
	}
	

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Product> findAll()
	{
		
		return productBs.findAll() ;
	}
	@RequestMapping(value = "/findbyid/{id}", 
			method = RequestMethod.GET)
	public Product findbyId(@PathVariable int id)
	{
		return productBs.findbyId(id)	;
	}
	
	
	@RequestMapping(value = "/delete/{id}", 
			method = RequestMethod.DELETE)
	public void delete(@PathVariable int id)
	{
		 productBs.delete(id)	;
	}

	@RequestMapping(value = "/save", 
			method = RequestMethod.POST)
	public int save(@RequestBody Product product)
	{
		return 	productBs.save(product);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
